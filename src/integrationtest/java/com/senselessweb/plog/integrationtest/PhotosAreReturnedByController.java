package com.senselessweb.plog.integrationtest;

import com.jayway.jsonpath.JsonPath;
import com.senselessweb.plog.AbstractIntegrationTest;
import com.senselessweb.plog.domain.Photo;
import com.senselessweb.plog.service.photo.repository.PhotoRepository;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.List;

public class PhotosAreReturnedByController extends AbstractIntegrationTest {

    private static File testphoto = new File("src/integrationtest/resources/photos/USA/20160517-DSCN2973.jpg");

    @Autowired
    private PhotoRepository photoRepository;

    @Test
    public void assertAllPhotosAreReturnedByController() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/photos/"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(11));
    }

    @Test
    public void assertPhotoIsReturnedCorrectly() throws Exception {
        final Photo storedPhoto = photoRepository.findByPhotofileAbsolutePath(testphoto.getAbsolutePath()).get();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/photos/" + storedPhoto.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Los Angeles"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.width").value("3595"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.height").value("1540"));
    }

    @Test
    public void assertThumbnailsAreAvailable() throws Exception {
        final Photo storedPhoto = photoRepository.findByPhotofileAbsolutePath(testphoto.getAbsolutePath()).get();
        final List<Integer> sizes = JsonPath.parse(this.mockMvc.perform(MockMvcRequestBuilders.get("/photos/" + storedPhoto.getId()))
                .andReturn().getResponse().getContentAsString()).read("$.thumbnails[*].size");
        for (final int size : sizes) {
            this.mockMvc.perform(MockMvcRequestBuilders.get("/photos/" + storedPhoto.getId() + "/thumbnail/" + size))
                    .andExpect(MockMvcResultMatchers.status().isOk())
                    .andExpect(MockMvcResultMatchers.content().contentType(MediaType.IMAGE_JPEG))
                    .andExpect(response -> {
                        BufferedImage image = ImageIO.read(new ByteArrayInputStream(response.getResponse().getContentAsByteArray()));
                        Assert.assertTrue(String.format("Thumbnail width should not exceed size of %s, but was %s", size, image.getWidth()), image.getWidth() <= size);
                        Assert.assertTrue(String.format("Thumbnail height should not exceed size of %s, but was %s", size, image.getWidth()), image.getHeight() <= size);
                    });
        }
    }

}
