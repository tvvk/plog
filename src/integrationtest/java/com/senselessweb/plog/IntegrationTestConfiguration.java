package com.senselessweb.plog;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;

@SpringBootApplication
@EnableScheduling
public class IntegrationTestConfiguration {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Primary
    @Bean("photoDirectory")
    public File photoDirectory() {
        return new File("src/integrationtest/resources/photos");
    }

    @Primary
    @Bean("thumbnailsDirectory")
    public File thumbnailsDirectory() throws IOException {
        File thumbnails = new File(System.getProperty("java.io.tmpdir"), "plog-thumbnails-" + System.currentTimeMillis() + "/");
        thumbnails.mkdirs();
        FileUtils.forceDeleteOnExit(thumbnails); // Still not deleted afterwards
        return thumbnails;
    }

    @PostConstruct
    public void setup() {
        this.mongoTemplate.getDb().drop();
    }
}
