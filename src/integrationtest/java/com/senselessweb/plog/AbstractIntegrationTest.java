package com.senselessweb.plog;

import com.senselessweb.plog.service.photo.thumbnails.ThumbnailsService;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest(
        properties = {
                "spring.data.mongodb.database=db_integrationtest",
                "spring.main.allow-bean-definition-overriding=true"
        },
        classes = IntegrationTestConfiguration.class)
@AutoConfigureMockMvc
public abstract class AbstractIntegrationTest {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    @Qualifier("thumbnailsDirectory")
    private File thumbnailsDirectory;

    @Autowired
    @Qualifier("photoDirectory")
    private File photoDirectory;

    @Before
    public void waitForThumbnails() throws InterruptedException {
        final int expectedNumberOfFile = FileUtils.listFiles(photoDirectory, new String[]{"jpg"}, true).size() * ThumbnailsService.thumbnailSizes.length;
        final long timeout = System.currentTimeMillis() + 30 * 1000;
        while (thumbnailsDirectory.listFiles().length < expectedNumberOfFile) {
            if (System.currentTimeMillis() > timeout) {
                Assert.fail(String.format("Still only %s thumbnails after 30 seconds (Expected: %s)",
                        thumbnailsDirectory.listFiles().length, expectedNumberOfFile));
            }
            Thread.sleep(10);
        }
    }

}
