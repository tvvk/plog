package com.senselessweb.plog;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class Directories {

    private final File photoDirectory;
    private final File thumbnailsDirectory;

    public Directories(
            @Qualifier("photoDirectory") File photoDirectory,
            @Qualifier("thumbnailsDirectory") File thumbnailsDirectory) {
        this.photoDirectory = photoDirectory;
        this.thumbnailsDirectory = thumbnailsDirectory;
    }

    public File getPhotoDirectory() {
        return photoDirectory;
    }

    public File getThumbnailsDirectory() {
        return thumbnailsDirectory;
    }

}
