package com.senselessweb.plog.service.util;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.time.Instant;

public class PhotoUtils {

    public static String getFilename(final File file) {
        return FilenameUtils.getName(file.getName());
    }

    public static Instant getLastModified(final File file) {
        return Instant.ofEpochMilli(file.lastModified());
    }
}
