package com.senselessweb.plog.service.photo.repository;

import com.senselessweb.plog.domain.Photo;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface PhotoRepository extends MongoRepository<Photo, String> {

    Optional<Photo> findByPhotofileAbsolutePath(String absolutePath);

    List<Photo> findByAlbumIdsContains(String albumId);
}
