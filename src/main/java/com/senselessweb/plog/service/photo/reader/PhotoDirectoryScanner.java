package com.senselessweb.plog.service.photo.reader;

import com.senselessweb.plog.Directories;
import com.senselessweb.plog.domain.Photo;
import com.senselessweb.plog.service.photo.PhotoService;
import com.senselessweb.plog.service.photo.thumbnails.ThumbnailsService;
import com.senselessweb.plog.service.util.PhotoUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Arrays;
import java.util.Optional;

@Service
public class PhotoDirectoryScanner {

    private static final Logger log = LoggerFactory.getLogger(PhotoDirectoryScanner.class.getCanonicalName());

    private final Directories directories;
    private final PhotoService photoService;
    private final ThumbnailsService thumbnailsService;

    public PhotoDirectoryScanner(Directories directories, PhotoService photoService, ThumbnailsService thumbnailsService) {
        this.directories = directories;
        this.photoService = photoService;
        this.thumbnailsService = thumbnailsService;
    }

    @Scheduled(fixedRate = 60 * 60 * 1000, initialDelay = 1000)
    public void scan() {
        log.info("Scanning...");
        this.scanDirectory(this.directories.getPhotoDirectory());
        log.info("Scanning finished");
    }

    private void scanDirectory(File directory) {
        log.info(String.format("Scanning directory %s", directory.getAbsolutePath()));
        Arrays.stream(directory.listFiles()).filter(this::needsRegeneration).forEach(this::regeneratePhoto);
        Arrays.stream(directory.listFiles()).filter(File::isDirectory).forEach(this::scanDirectory);
    }

    private void regeneratePhoto(File file) {
        log.info(String.format("Regenerating %s", file.getAbsolutePath()));
        this.photoService.findByFile(file).ifPresent(this.photoService::delete);
        try {
            this.photoService.createFromFile(file);
        } catch (Exception e) {
            log.error(String.format("Can not load photo metadata, Skipping photo %s ...", file), e);
        }
    }

    private boolean needsRegeneration(File file) {
        return isPhotofile(file) && (isOutdated(file) || !allThumbnailsAreAvailable(file));
    }

    private boolean allThumbnailsAreAvailable(File file) {
        final Optional<Photo> photo = this.photoService.findByFile(file);
        // TODO What if thumbnails are in generation?
        return photo.isPresent() && !thumbnailsService.getThumbnails(photo.get()).getThumbnails().isEmpty();
    }

    private boolean isOutdated(File file) {
        return this.photoService.findByFile(file)
                .map(photo -> photo.getPhotofile().getLastModified().isBefore(PhotoUtils.getLastModified(file)))
                .orElse(true);
    }

    private boolean isPhotofile(File file) {
        return file.isFile() && FilenameUtils.isExtension(PhotoUtils.getFilename(file), "jpg");
    }
}
