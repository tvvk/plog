package com.senselessweb.plog.service.photo;

import com.senselessweb.plog.domain.Photo;
import com.senselessweb.plog.service.photo.reader.PhotoReader;
import com.senselessweb.plog.service.photo.repository.PhotoRepository;
import com.senselessweb.plog.service.photo.thumbnails.ThumbnailsAsyncFlowGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.Optional;

@Service
public class PhotoService {

    private static final Logger log = LoggerFactory.getLogger(PhotoService.class.getCanonicalName());

    private final PhotoRepository photoRepository;
    private final PhotoReader photoReader;
    private final ThumbnailsAsyncFlowGateway thumbnails;

    public PhotoService(PhotoRepository photoRepository, PhotoReader photoReader, ThumbnailsAsyncFlowGateway thumbnails) {
        this.photoRepository = photoRepository;
        this.photoReader = photoReader;
        this.thumbnails = thumbnails;
    }

    public Optional<Photo> findByFile(File file) {
        return photoRepository.findByPhotofileAbsolutePath(file.getAbsolutePath());
    }

    public void delete(Photo photo) {
        this.photoRepository.delete(photo);
    }

    public Photo createFromFile(File file) throws Exception {
        final Photo photo = this.photoRepository.save(this.photoReader.readPhoto(file));
        this.thumbnails.createThumbnails(photo);
        return photo;
    }

    public List<Photo> getAllPhotos() {
        return this.photoRepository.findAll();
    }

    public Optional<Photo> getPhoto(String id) {
        return this.photoRepository.findById(id);
    }

    public List<Photo> getAlbumPhotos(String albumId) {
        return this.photoRepository.findByAlbumIdsContains(albumId);
    }
}
