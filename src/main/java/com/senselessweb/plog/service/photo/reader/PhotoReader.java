package com.senselessweb.plog.service.photo.reader;

import com.adobe.xmp.XMPException;
import com.adobe.xmp.properties.XMPPropertyInfo;
import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.GpsDirectory;
import com.drew.metadata.iptc.IptcDirectory;
import com.drew.metadata.jpeg.JpegDirectory;
import com.drew.metadata.xmp.XmpDirectory;
import com.google.common.collect.Streams;
import com.senselessweb.plog.domain.Album;
import com.senselessweb.plog.domain.Dimension;
import com.senselessweb.plog.domain.Photo;
import com.senselessweb.plog.domain.Position;
import com.senselessweb.plog.service.album.AlbumService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PhotoReader {

    private static final Logger log = LoggerFactory.getLogger(PhotoReader.class.getCanonicalName());

    private final AlbumService albumService;

    public PhotoReader(AlbumService albumService) {
        this.albumService = albumService;
    }

    public Photo readPhoto(File file) throws Exception {
        final Metadata metadata = readMetadata(file);
        return new Photo(readTitle(metadata), readPosition(metadata), readAlbumIds(metadata), file, readDimension(metadata));
    }

    private Metadata readMetadata(File file) throws Exception {
        final Metadata metadata = ImageMetadataReader.readMetadata(file);
        if (log.isDebugEnabled()) {
            log.debug("= Reading metadata for: " + file);
            metadata.getDirectories().forEach(directory -> {
                log.debug("== Directory: " + directory.getName());
                directory.getTags().forEach(tag -> log.debug("=== Tag: " + tag));
            });
        }
        return metadata;
    }

    private List<String> readAlbumIds(Metadata metadata) throws XMPException {
        final Stream<XMPPropertyInfo> stream = Streams.stream(metadata.getFirstDirectoryOfType(XmpDirectory.class).getXMPMeta().iterator());
        return stream.filter(prop -> StringUtils.startsWith(((XMPPropertyInfo) prop).getPath(), "lr:hierarchicalSubject["))
                .filter(prop -> StringUtils.startsWith(((XMPPropertyInfo) prop).getValue(), "WebExport|Album|"))
                .map(prop -> StringUtils.substringAfter(((XMPPropertyInfo) prop).getValue(), "WebExport|Album|"))
                .map(this.albumService::getOrCreateAlbum)
                .map(Album::getId)
                .collect(Collectors.toList());
    }

    private String readTitle(Metadata metadata) {
        return metadata.getFirstDirectoryOfType(IptcDirectory.class).getString(IptcDirectory.TAG_OBJECT_NAME);
    }

    private Dimension readDimension(Metadata metadata) throws MetadataException {
        JpegDirectory directory = metadata.getFirstDirectoryOfType(JpegDirectory.class);
        return new Dimension(directory.getImageWidth(), directory.getImageHeight());
    }

    private Position readPosition(Metadata metadata) {
        final GpsDirectory gpsDirectory = metadata.getFirstDirectoryOfType(GpsDirectory.class);
        return Optional.ofNullable(gpsDirectory).map(GpsDirectory::getGeoLocation).map(Position::new).orElse(null);
    }
}
