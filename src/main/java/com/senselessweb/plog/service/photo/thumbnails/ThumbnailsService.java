package com.senselessweb.plog.service.photo.thumbnails;

import com.senselessweb.plog.Directories;
import com.senselessweb.plog.domain.Photo;
import com.senselessweb.plog.domain.Thumbnails;
import com.senselessweb.plog.service.photo.reader.PhotoDirectoryScanner;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public
class ThumbnailsService {

    private static final Logger log = LoggerFactory.getLogger(PhotoDirectoryScanner.class.getCanonicalName());
    public static final int[] thumbnailSizes = new int[]{128, 1024, 1920};

    private final Directories directories;

    public ThumbnailsService(Directories directories) {
        this.directories = directories;
    }

    public Thumbnails getThumbnails(final Photo photo) {
        final List<Thumbnails.Thumbnail> thumbnails = Arrays.stream(thumbnailSizes)
                .mapToObj(size -> ImmutablePair.of(size, storedFile(photo, size)))
                .filter(pair -> pair.getRight().isFile())
                .map(pair -> new Thumbnails.Thumbnail(pair.getLeft(), pair.getRight()))
                .collect(Collectors.toList());
        return new Thumbnails(photo, thumbnails);
    }

    void deleteThumbnails(Photo photo) {
        log.debug(String.format("Deleting thumbnails for %s", photo));
        Arrays.stream(thumbnailSizes).forEach(size -> storedFile(photo, size).delete());
    }

    void generateThumbnails(Photo photo) {
        log.debug(String.format("Writing thumbnails for %s", photo));
        try {
            final BufferedImage image = ImageIO.read(photo.getPhotofile().getFile());
            Arrays.stream(thumbnailSizes).forEach(size -> generateThumbnail(photo, image, size));
        } catch (final Exception e) {
            log.error(String.format("Could not generate thumbnails for %s. Skipping...", photo), e);
        }
    }

    private void generateThumbnail(Photo photo, BufferedImage image, int size) {
        final File storedFile = storedFile(photo, size);
        log.debug(String.format("Writing thumbnail in size %s for %s to %s", size, photo, storedFile));
        try {
            ImageIO.write(Scalr.resize(image, size), "jpg", storedFile);
        } catch (final Exception e) {
            log.error(String.format("Could not generate thumbnail for %s in size %s. Skipping...", photo, size), e);
        }
    }

    private File storedFile(Photo photo, int size) {
        return new File(directories.getThumbnailsDirectory(),
                FilenameUtils.getBaseName(photo.getId()) + "@" + size + ".jpg");
    }

}
