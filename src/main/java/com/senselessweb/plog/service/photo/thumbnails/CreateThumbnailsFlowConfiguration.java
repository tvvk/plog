package com.senselessweb.plog.service.photo.thumbnails;

import com.senselessweb.plog.domain.Photo;
import com.senselessweb.plog.service.photo.repository.PhotoRepository;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.amqp.dsl.Amqp;
import org.springframework.integration.amqp.outbound.AmqpOutboundEndpoint;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.messaging.MessageHeaders;

@Configuration
@IntegrationComponentScan(basePackageClasses = CreateThumbnailsFlowConfiguration.class)
public class CreateThumbnailsFlowConfiguration {

    public static final String CHANNEL_AMQP_CREATE_THUMBNAILS_OUTBOUND = "channel.create.thumbnails.outbound";
    public static final String AMQP_QUEUE_CREATE_THUMBNAILS = "queue.plog.create.thumbnails";

    private final ThumbnailsService thumbnailsService;
    private final PhotoRepository photoRepository;

    public CreateThumbnailsFlowConfiguration(ThumbnailsService thumbnailsService, PhotoRepository photoRepository) {
        this.thumbnailsService = thumbnailsService;
        this.photoRepository = photoRepository;
    }

    @Bean
    @ServiceActivator(inputChannel = CHANNEL_AMQP_CREATE_THUMBNAILS_OUTBOUND)
    public AmqpOutboundEndpoint amqpCreateThumbnailsOutbound(AmqpTemplate amqpTemplate) {
        final AmqpOutboundEndpoint outbound = new AmqpOutboundEndpoint(amqpTemplate);
        outbound.setRoutingKey(AMQP_QUEUE_CREATE_THUMBNAILS);
        return outbound;
    }

    @Bean
    public IntegrationFlow flowCreateThumbnailsOutbound() {
        return IntegrationFlows.from(ThumbnailsAsyncFlowGateway.FLOW_CREATE_THUMBNAILS)
                .transform(this::createOutboundMessage)
                .log(LoggingHandler.Level.INFO)
                .channel(CHANNEL_AMQP_CREATE_THUMBNAILS_OUTBOUND)
                .get();
    }

    @Bean
    public IntegrationFlow flowCreateThumbnailsInbound(ConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Amqp.inboundAdapter(connectionFactory, AMQP_QUEUE_CREATE_THUMBNAILS))
                .log(LoggingHandler.Level.INFO)
                .handle(this::handleInboundMessage)
                .get();
    }

    private String createOutboundMessage(Photo photo) {
        return photo.getId();
    }

    private Void handleInboundMessage(String payload, MessageHeaders messageHeaders) {
        this.thumbnailsService.generateThumbnails(this.photoRepository.findById(payload).get());
        return null;
    }
}
