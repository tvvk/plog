package com.senselessweb.plog.service.photo.thumbnails;

import com.senselessweb.plog.domain.Photo;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway
public interface ThumbnailsAsyncFlowGateway {

    String FLOW_CREATE_THUMBNAILS = "flow.create.thumbnails";

    @Gateway(requestChannel = FLOW_CREATE_THUMBNAILS)
    void createThumbnails(Photo photo);
}
