package com.senselessweb.plog.service.album.repository;

import com.senselessweb.plog.domain.Album;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface AlbumRepository extends MongoRepository<Album, String> {

    Optional<Album> findByName(String name);
}
