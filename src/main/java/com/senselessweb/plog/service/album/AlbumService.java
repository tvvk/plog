package com.senselessweb.plog.service.album;

import com.senselessweb.plog.domain.Album;
import com.senselessweb.plog.service.album.repository.AlbumRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class AlbumService {

    private final AlbumRepository albumRepository;

    public AlbumService(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    public synchronized Album getOrCreateAlbum(String name) {
        return this.albumRepository.findByName(name).orElseGet(() -> albumRepository.save(new Album(name)));
    }

    public Collection<Album> getAllAlbums() {
        return this.albumRepository.findAll();
    }
}
