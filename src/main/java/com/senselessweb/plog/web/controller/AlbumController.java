package com.senselessweb.plog.web.controller;

import com.senselessweb.plog.service.album.AlbumService;
import com.senselessweb.plog.web.model.WebAlbum;
import com.senselessweb.plog.web.support.Converters;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("/albums")
public class AlbumController {

    private final AlbumService albumService;
    private final Converters converters;

    public AlbumController(AlbumService albumService, Converters converters) {
        this.albumService = albumService;
        this.converters = converters;
    }

    @GetMapping
    public Stream<WebAlbum> getAllAlbums() {
        return this.albumService.getAllAlbums().stream().map(this.converters::convertAlbum);
    }
}
