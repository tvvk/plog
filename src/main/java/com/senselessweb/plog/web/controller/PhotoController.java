package com.senselessweb.plog.web.controller;

import com.senselessweb.plog.domain.Photo;
import com.senselessweb.plog.service.photo.PhotoService;
import com.senselessweb.plog.service.photo.thumbnails.ThumbnailsService;
import com.senselessweb.plog.web.exception.PhotoNotFoundException;
import com.senselessweb.plog.web.model.WebPhoto;
import com.senselessweb.plog.web.support.Converters;
import org.apache.commons.io.FileUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;

@RestController
@RequestMapping("/photos")
public class PhotoController {

    private final PhotoService photoService;
    private final ThumbnailsService thumbnailsService;
    private final Converters converters;

    public PhotoController(PhotoService photoService, ThumbnailsService thumbnailsService, Converters converters) {
        this.photoService = photoService;
        this.thumbnailsService = thumbnailsService;
        this.converters = converters;
    }

    @RequestMapping("/")
    public Stream<WebPhoto> getAllPhotos() {
        return photoService.getAllPhotos().stream().map(this.converters::convertPhoto);
    }

    @RequestMapping("/{id}")
    public WebPhoto getPhotos(@PathVariable("id") String id) {
        return photoService.getPhoto(id).map(this.converters::convertPhoto).orElseThrow(PhotoNotFoundException::new);
    }

    @RequestMapping("/{id}/thumbnail/{size}")
    public ResponseEntity<byte[]> getThumbnail(@PathVariable("id") String id, @PathVariable("size") int size) throws IOException {
        final Photo photo = this.photoService.getPhoto(id).orElseThrow(PhotoNotFoundException::new);
        final File thumbnail = thumbnailsService.getThumbnails(photo).getThumbnail(size).getFile();
        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(FileUtils.readFileToByteArray(thumbnail));
    }

}
