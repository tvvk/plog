package com.senselessweb.plog.web.model;

import com.senselessweb.plog.domain.Album;

public class WebAlbum {

    private final Album album;
    private final WebPhoto albumPhoto;

    public WebAlbum(Album album, WebPhoto albumPhoto) {
        this.album = album;
        this.albumPhoto = albumPhoto;
    }

    public String getId() {
        return this.album.getId();
    }

    public String getName() {
        return this.album.getName();
    }

    public WebPhoto getAlbumPhoto() {
        return albumPhoto;
    }
}
