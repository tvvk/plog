package com.senselessweb.plog.web.model;

import com.senselessweb.plog.domain.Thumbnails;

public class WebThumbnail {

    private final Thumbnails.Thumbnail thumbnail;

    public WebThumbnail(Thumbnails.Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getSize() {
        return thumbnail.getSize();
    }
}
