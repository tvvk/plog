package com.senselessweb.plog.web.model;

import com.senselessweb.plog.domain.Photo;
import com.senselessweb.plog.domain.Position;
import com.senselessweb.plog.domain.Thumbnails;
import com.senselessweb.plog.service.util.PhotoUtils;

import java.util.stream.Stream;

public class WebPhoto {

    private final Photo photo;
    private final Thumbnails thumbnails;

    public WebPhoto(Photo photo, Thumbnails thumbnails) {
        this.photo = photo;
        this.thumbnails = thumbnails;
    }

    public String getFilename() {
        return PhotoUtils.getFilename(photo.getPhotofile().getFile());
    }

    public String getTitle() {
        return photo.getTitle();
    }

    public String getId() {
        return photo.getId();
    }

    public int getWidth() {
        return photo.getDimension().getWidth();
    }

    public int getHeight() {
        return photo.getDimension().getHeight();
    }

    public Double getLongitude() {
        return photo.getPosition().map(Position::getLongitude).orElse(null);
    }

    public Double getLatitude() {
        return photo.getPosition().map(Position::getLatitude).orElse(null);
    }

    public Stream<WebThumbnail> getThumbnails() {
        return thumbnails.getThumbnails().stream().map(WebThumbnail::new);
    }
}
