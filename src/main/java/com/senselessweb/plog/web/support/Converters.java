package com.senselessweb.plog.web.support;

import com.senselessweb.plog.domain.Album;
import com.senselessweb.plog.domain.Photo;
import com.senselessweb.plog.service.photo.PhotoService;
import com.senselessweb.plog.service.photo.thumbnails.ThumbnailsService;
import com.senselessweb.plog.web.model.WebAlbum;
import com.senselessweb.plog.web.model.WebPhoto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class Converters {

    private final ThumbnailsService thumbnailsService;
    private final PhotoService photoService;

    public Converters(ThumbnailsService thumbnailsService, PhotoService photoService) {
        this.thumbnailsService = thumbnailsService;
        this.photoService = photoService;
    }

    public WebPhoto convertPhoto(Photo photo) {
        return new WebPhoto(photo, thumbnailsService.getThumbnails(photo));
    }

    public WebAlbum convertAlbum(Album album) {
        return new WebAlbum(album, getAlbumPhoto(album).map(this::convertPhoto).orElse(null));
    }

    private Optional<Photo> getAlbumPhoto(Album album) {
        if (album.getAlbumPhotoId().isPresent())
            return photoService.getPhoto(album.getAlbumPhotoId().get());

        final List<Photo> allPhotos = photoService.getAlbumPhotos(album.getId());
        return allPhotos.isEmpty() ? Optional.empty() : Optional.of(allPhotos.get(0));
    }


}
