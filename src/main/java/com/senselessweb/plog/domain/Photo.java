package com.senselessweb.plog.domain;

import lombok.*;
import org.springframework.data.annotation.Id;

import java.io.File;
import java.util.List;
import java.util.Optional;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class Photo {

    @Id
    private String id;

    private String title;
    private Position position;
    private Photofile photofile;
    private List<String> albumIds;
    private Dimension dimension;

    public Photo(
            final String title,
            final Position position,
            final List<String> albumIds,
            final File file,
            final Dimension dimension) {
        this(null, title, position, new Photofile(file), albumIds, dimension);
    }

    public Optional<Position> getPosition() {
        return Optional.ofNullable(this.position);
    }

}
