package com.senselessweb.plog.domain;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class Dimension {

    private int width;
    private int height;
}
