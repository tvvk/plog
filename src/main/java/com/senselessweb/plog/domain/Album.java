package com.senselessweb.plog.domain;

import lombok.*;
import org.springframework.data.annotation.Id;

import java.util.Optional;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class Album {

    private String id;
    private String albumPhotoId;

    @Id
    private String name;

    public Album(String name) {
        this.name = name;
    }

    public Optional<String> getAlbumPhotoId() {
        return Optional.ofNullable(albumPhotoId);
    }
}
