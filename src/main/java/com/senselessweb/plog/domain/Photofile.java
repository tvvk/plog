package com.senselessweb.plog.domain;

import com.senselessweb.plog.service.util.PhotoUtils;
import lombok.*;

import java.io.File;
import java.io.Serializable;
import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class Photofile implements Serializable {

    private String absolutePath;
    private Instant lastModified;

    public Photofile(File file) {
        this(file.getAbsolutePath(), PhotoUtils.getLastModified(file));
    }

    public File getFile() {
        return new File(getAbsolutePath());
    }
}
