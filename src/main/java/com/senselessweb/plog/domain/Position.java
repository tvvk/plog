package com.senselessweb.plog.domain;

import com.drew.lang.GeoLocation;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class Position implements Serializable {

    private double latitude;
    private double longitude;

    public Position(GeoLocation geoLocation) {
        this(geoLocation.getLatitude(), geoLocation.getLongitude());
    }
}
