package com.senselessweb.plog.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@ToString
@EqualsAndHashCode
public class Thumbnails {

    @AllArgsConstructor
    @Getter
    @ToString
    @EqualsAndHashCode
    public static class Thumbnail {

        private final int size;
        private final File file;
    }

    private final Photo photo;
    private final List<Thumbnail> thumbnailsBySizeAscending;

    public Thumbnails(Photo photo, List<Thumbnail> thumbnails) {
        this.photo = photo;
        this.thumbnailsBySizeAscending = new ArrayList<>(thumbnails);
        Collections.sort(this.thumbnailsBySizeAscending, Comparator.comparingInt(Thumbnail::getSize));
    }

    public List<Thumbnail> getThumbnails() {
        return thumbnailsBySizeAscending;
    }

    public Thumbnail getThumbnail(int size) {
        return thumbnailsBySizeAscending.stream().filter(t -> t.getSize() >= size).findFirst()
                .orElse(this.thumbnailsBySizeAscending.get(this.thumbnailsBySizeAscending.size() - 1));
    }
}
