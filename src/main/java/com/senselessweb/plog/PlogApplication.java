package com.senselessweb.plog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.File;

@SpringBootApplication
@EnableScheduling
public class PlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlogApplication.class, args);
    }

    @Bean("photoDirectory")
    public File photoDirectory() {
        return new File("/var/plog/images/");
    }

    @Bean("thumbnailsDirectory")
    public File thumbnailsDirectory() {
        final File thumbnails = new File("/var/plog/thumbnails/");
        if (!thumbnails.isDirectory()) {
            if (!thumbnails.mkdirs()) {
                throw new IllegalStateException(String.format("Could not create thumbnails directory %s", thumbnails));
            }
        }
        return thumbnails;
    }
}
