package com.senselessweb.plog.domain;

import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;

public class ThumbnailsTest {

    @Test
    public void assertCorrectThumbnailSizeIsReturned() {
        Thumbnails testee = new Thumbnails(Mockito.mock(Photo.class), Lists.newArrayList(
                new Thumbnails.Thumbnail(64, new File("image@64.jpg")),
                new Thumbnails.Thumbnail(128, new File("image@128.jpg")),
                new Thumbnails.Thumbnail(1024, new File("image@1024.jpg"))
        ));
        Assert.assertEquals("image@64.jpg", testee.getThumbnail(63).getFile().getName());
        Assert.assertEquals("image@64.jpg", testee.getThumbnail(64).getFile().getName());
        Assert.assertEquals("image@128.jpg", testee.getThumbnail(65).getFile().getName());
        Assert.assertEquals("image@1024.jpg", testee.getThumbnail(1025).getFile().getName());

    }

}