package com.senselessweb.plog.service.photo.reader;

import com.senselessweb.plog.Directories;
import com.senselessweb.plog.domain.Photo;
import com.senselessweb.plog.domain.Photofile;
import com.senselessweb.plog.domain.Thumbnails;
import com.senselessweb.plog.service.album.AlbumService;
import com.senselessweb.plog.service.photo.PhotoService;
import com.senselessweb.plog.service.photo.thumbnails.ThumbnailsService;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.util.Optional;

public class PhotoDirectoryScannerTest {

    private PhotoDirectoryScanner testee;
    private PhotoService photoService;

    private File photoFile1 = new File("src/test/resources/test-photo-directory/album1/20140926-DSC_3887.jpg");
    private File photoFile2 = new File("src/test/resources/test-photo-directory/album2/20180518-DSC_0417.jpg");
    private Photo photo1;
    private Photo photo2;

    @Before
    public void setup() throws Exception {
        final PhotoReader photoReader = new PhotoReader(Mockito.mock(AlbumService.class));
        this.photo1 = photoReader.readPhoto(photoFile1);
        this.photo2 = photoReader.readPhoto(photoFile2);
        this.photoService = Mockito.mock(PhotoService.class);
        Mockito.when(this.photoService.findByFile(photoFile1)).thenReturn(Optional.of(photo1));
        Mockito.when(this.photoService.findByFile(photoFile2)).thenReturn(Optional.of(photo2));
        final Directories directories = new Directories(new File("src/test/resources/test-photo-directory"), null);
        final ThumbnailsService thumbnailsService = Mockito.mock(ThumbnailsService.class);
        Mockito.when(thumbnailsService.getThumbnails(Mockito.any())).thenReturn(new Thumbnails(
                this.photo1, Lists.newArrayList(new Thumbnails.Thumbnail(128, new File(".")))));
        this.testee = new PhotoDirectoryScanner(directories, photoService, thumbnailsService);
    }

    @Test
    public void assertUnchangedPhotosAreNotDeletedOrSaved() throws Exception {
        this.testee.scan();
        Mockito.verify(this.photoService, Mockito.never()).delete(Mockito.any());
        Mockito.verify(this.photoService, Mockito.never()).createFromFile(Mockito.any());
    }

    @Test
    public void assertNotExistingPhotosAreSaved() throws Exception {
        Mockito.when(this.photoService.findByFile(Mockito.any())).thenReturn(Optional.empty());
        this.testee.scan();
        Mockito.verify(this.photoService).createFromFile(photoFile1);
        Mockito.verify(this.photoService).createFromFile(photoFile2);
    }

    @Test
    public void assertOnlyChangedFilesAreDeletedAndSaved() throws Exception {
        final Photo outdatedPhoto2 = new Photo(null, photo2.getTitle(), photo2.getPosition().get(), new Photofile(photo2.getPhotofile().getFile().getAbsolutePath(), photo2.getPhotofile().getLastModified().minusSeconds(10)),
                photo2.getAlbumIds(), null);
        Mockito.when(this.photoService.findByFile(photoFile2)).thenReturn(Optional.of(outdatedPhoto2));
        this.testee.scan();
        Mockito.verify(this.photoService).delete(outdatedPhoto2);
        Mockito.verify(this.photoService).createFromFile(photoFile2);
    }

}