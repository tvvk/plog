package com.senselessweb.plog.service.photo.reader;

import com.google.common.collect.Lists;
import com.senselessweb.plog.domain.Album;
import com.senselessweb.plog.domain.Dimension;
import com.senselessweb.plog.domain.Photo;
import com.senselessweb.plog.service.album.AlbumService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;

public class PhotoReaderTest {

    private static final File testphoto_with_gps = new File("src/test/resources/test-images/with-gps.jpg");
    private static final File testphoto_without_gps = new File("src/test/resources/test-images/without-gps.jpg");
    private static final File testphoto_with_export_tags = new File("src/test/resources/test-images/with-export-tags.jpg");

    private AlbumService albumService;
    private Album denmark;
    private PhotoReader testee;

    @Before
    public void setup() {
        this.albumService = Mockito.mock(AlbumService.class);
        this.denmark = Mockito.mock(Album.class);
        Mockito.when(this.denmark.getId()).thenReturn("42");
        Mockito.when(this.albumService.getOrCreateAlbum("Dänemark")).thenReturn(denmark);
        this.testee = new PhotoReader(albumService);
    }


    @Test
    public void assertTitleCanBeRead() throws Exception {
        Assert.assertEquals("Maritimes Museum Hamburg", this.testee.readPhoto(testphoto_with_gps).getTitle());
    }

    @Test
    public void assertDimensionCanBeRead() throws Exception {
        Assert.assertEquals(new Dimension(3576, 2682), this.testee.readPhoto(testphoto_with_gps).getDimension());
    }

    @Test
    public void assertGpsPositionCanBeRead() throws Exception {
        Photo photo = this.testee.readPhoto(testphoto_with_gps);
        Assert.assertEquals(53.543853, photo.getPosition().get().getLatitude(), 0.01);
        Assert.assertEquals(9.998335, photo.getPosition().get().getLongitude(), 0.01);
    }

    @Test
    public void assertAlbumTagsCanBeRead() throws Exception {
        final Photo photo = this.testee.readPhoto(testphoto_with_export_tags);
        Assert.assertEquals(Lists.newArrayList("42"), photo.getAlbumIds());
    }


}