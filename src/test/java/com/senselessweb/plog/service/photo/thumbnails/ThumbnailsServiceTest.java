package com.senselessweb.plog.service.photo.thumbnails;

import com.senselessweb.plog.Directories;
import com.senselessweb.plog.domain.Photo;
import com.senselessweb.plog.service.album.AlbumService;
import com.senselessweb.plog.service.photo.reader.PhotoReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

import java.io.File;
import java.util.Arrays;


public class ThumbnailsServiceTest {

    @Rule
    public TemporaryFolder thumbnailsDirRule = new TemporaryFolder();

    private ThumbnailsService testee;
    private File thumbnailsDir;
    private Photo photo;

    @Before
    public void setup() throws Exception {
        this.thumbnailsDir = thumbnailsDirRule.newFolder();
        final Directories directories = new Directories(new File("src/test/resources/test-photo-directory"), thumbnailsDir);
        this.testee = new ThumbnailsService(directories);
        final PhotoReader photoReader = new PhotoReader(Mockito.mock(AlbumService.class));
        this.photo = photoReader.readPhoto(new File("src/test/resources/test-photo-directory/album2/20180518-DSC_0417.jpg"));
    }

    @Test
    public void assertThumbnailsCanBeCreated() {
        this.testee.generateThumbnails(photo);
        Arrays.stream(ThumbnailsService.thumbnailSizes).forEach(
                size -> Assert.assertTrue(new File(thumbnailsDir, this.photo.getId() + "@" + size + ".jpg").length() > 0));
    }

    @Test
    public void assertThumbnailsCanBeDeleted() {
        this.testee.generateThumbnails(photo);
        this.testee.deleteThumbnails(photo);
        Arrays.stream(ThumbnailsService.thumbnailSizes).forEach(
                size -> Assert.assertFalse(new File(thumbnailsDir, "20180518-DSC_0417@" + size + ".jpg").isFile()));
    }
}