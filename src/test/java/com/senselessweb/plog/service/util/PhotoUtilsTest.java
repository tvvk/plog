package com.senselessweb.plog.service.util;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class PhotoUtilsTest {

    private static final File testphoto = new File("src/test/resources/test-photo-directory/20180518-DSC_0417.jpg");

    @Test
    @Ignore
    public void assertTimestampIsCorrect() {
        Assert.assertEquals(
                LocalDateTime.of(2018, 11, 04, 11, 20, 9),
                LocalDateTime.ofInstant(PhotoUtils.getLastModified(testphoto), ZoneId.of("Europe/Berlin")));
    }

    @Test
    public void assertFilenameIsCorrect() {
        Assert.assertEquals("20180518-DSC_0417.jpg", PhotoUtils.getFilename(testphoto));
    }

}