FROM openjdk:8-jdk-alpine

COPY build/libs/plog-0.0.1-SNAPSHOT.jar app.jar

VOLUME /var/plog/images/
VOLUME /var/plog/thumbnails/

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]